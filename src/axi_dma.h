#ifndef AXI_DMA_H_
#define AXI_DMA_H_

#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <error.h>

/*!
 * \file axi_dma.h
 */

/* Invalid size */
#define GANNET_E_INSIZ -1
/* Invalid offset */
#define GANNET_E_INOFF -2
/* Invalid Direction */
#define GANNET_E_INDIR -3
/* Memory overflow */
#define GANNET_E_MEMOVER -4
/* DMA Internal Error */
#define GANNET_E_DMAINT -5
/* DMA Slave Error */
#define GANNET_E_SLAVE -6
/* DMA Decode Error */
#define GANNET_E_DECDE -7

//! An enumeration of the DMA transfer direction.
/*!
 * Enumeration   | Meaning
 * ------------- | -------------------
 * MM2S_TRANSFER | From memory into PL
 * S2MM_TRANSFER | From PL into memory
 */
typedef enum {
    MM2S_TRANSFER=0,
    S2MM_TRANSFER=1,
} dma_direction;

//! Each DMA device is represented by an instantiation of this structure.
/*!
 * This structure includes pointers to and sizes of the configuration register
 * space, and the device memory (into which it can DMA). It provides the
 * physical address of the device memory as the DMA controller must use this
 * rather than the virtual address used by libgannet. Finally, it includes the
 * file descriptor pointing to the device and its capability (whether it can
 * do MM2S, S2MM or both).
 */
struct dma_device_s;
typedef struct dma_device_s dma_device_t;

//! A function to cleanup the DMA device when the program has finished using it.
/*!
 * This function should be called when the program has finished with the DMA
 * device. For every ::gannet_device_init there should be one
 * ::gannet_device_cleanup.
 *
 * Call this function with the device passed as
 * an argument. It will unmap the memory and close the device file which were
 * mapped and opened as part of ::gannet_device_init.
 *
 * Return values     | Meaning
 * ----------------- | ------------------------------------
 * 0                 | Success
 * Other             | Error (Code passed on from munmap)
 */
int gannet_device_cleanup(dma_device_t *dma_device);

//! A function to initiate the DMA device.
/*!
 * Given the system file related to the DMA device, this function
 * extracts all the required information and returns a pointer to a
 * DMA device structure.
 *
 * This function should be run first before attempting any DMA transfers.
 *
 * In the case of failure this function will return NULL and will call the
 * ::gannet_device_cleanup.
 */
dma_device_t* gannet_device_init(const char *device_path);

//! A function to do a DMA transfer.
/*!
 * This function will perform a DMA transfer as specified in the arguments
 * passed to it. After calling this function the program should call
 * ::gannet_dma_interrupt_wait which will block until the transfer is
 * complete.
 *
 * It checks that the arguments passed to it are valid and returns errors if
 * not.
 * Return values     | Meaning
 * ----------------- | ------------------------------------
 * 0                 | Success
 * EINVAL            | Invalid nbytes (not a multiple of 8 in range 8 -> 1<SUP>23</SUP> -1)
 * EFAULT            | Invalid offset (not a multiple of 8 in the memory address range)
 * ENOPROTOOPT       | Invalid direction (The DMA device does not support the direction)
 * EMSGSIZE          | Combination of offset and nbytes will cause transfer to overrun the end of the memory
 *
 *  dma_transfer_direction should be of ::dma_direction.
 *
 *  Offset is the number of bytes offset within the memory which the DMA
 *  controller can access.
 */
int gannet_do_dma(dma_device_t *dma_device,
        dma_direction dma_transfer_direction, size_t offset,
        uint32_t nbytes);

//! A function which waits until the DMA transfer has completed.
/*!
 * This function waits for the transfer that was performed by ::gannet_do_dma
 * to complete and then resets the channel, ready for another transfer.
 */
int gannet_dma_interrupt_wait(dma_device_t *dma_device,
        dma_direction dma_transfer_direction, int32_t timeout_msec);

//! A function which returns a pointer to the device memory.
/*!
 * This function returns a pointer to the device memory allowing the calling
 * program to directly write the data for the DMA transfer or directly read
 * the data from the DMA transfer.
 */
void* gannet_get_memory(dma_device_t *dma_device);

//! A function to reset the DMA device.
/*!
 * This function will reset the DMA device, even if it is waiting for
 * completion of a transfer.
 */
int  gannet_dma_reset(dma_device_t *dma_device,
        dma_direction dma_transfer_direction);

#endif //Header guard
