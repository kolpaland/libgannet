.. libgannet documentation master file, created by
   sphinx-quickstart on Tue Feb 16 10:53:10 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to libgannet's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 2
   
   test/specification


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

