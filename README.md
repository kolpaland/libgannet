This README file contains information on the contents of libgannet.

Please see the corresponding sections below for details.

This is designed to run on an embedded Linux system. In our implementation we
used Yocto Jethro with the meta-xilinx layer and some custom add ons (which do
not affect libgannet):

    URI: git://git.yoctoproject.org/meta-xilinx
    branch: jethro


Dependencies
============

This library depends on:


Patches
=======

Please submit any patches against libgannet to the maintainer:

Maintainer: Toby Gomersall <toby.gomersall@smartacoustics.co.uk>


Table of Contents
=================

1. Testing
2. Building
3. Development and usage
4. Licensing

Testing
=======

To run the full suite of tests on libgannet:

`scons test`

To run libgannet under valgrind:

`scons valgrind`

To run a data rate test on libgannet:

`scons timer`

To run a specific test (after building):

`python -m unittest test.test_dma.TestDmaInit.<test name>`

The vivado project containing the loopback PL can be found here:

    URI: git@gitlab.com:toby.gomersall/dma_loopback_pl.git
    branch: master


Building
========

To build for release:

`scons release`

To build for test without running the tests:

`scons build_test`


Development and Usage
=====================
Please see this <a href="https://tobygomersallwordpresscom.wordpress.com/2016/02/11/xilinx-zynq-using-dma-to-transfer-data-to-the-linux-userspace/">blog post</a> for an explanation of the design.

Examples of the required extras can be found in libgannet/examples.


Licensing
=========

This library is licensed under the GPL. Commercial licenses can be purchased for a one time fee of £10,000 (plus VAT). Please contact Toby Gomersall (<toby.gomersall@smartacoustics.co.uk>).
