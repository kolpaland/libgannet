import unittest
import time
import errno
from py_axi_dma_wrapper.axi_dma import DMA, errors, direction
from contextlib import nested

import mmap
import numpy as np

class TestDmaInit(unittest.TestCase):
    def setUp(self):
        # Instantiate DMA devices
        self.mm2s_device = DMA("/dev/axi_dma_mm2s")
        self.s2mm_device = DMA("/dev/axi_dma_s2mm")

        # Define the size of the mm2s and s2mm memories
        self.mm2s_mem_size = 2**25
        self.s2mm_mem_size = 2**25

        # Define the max transfer size
        self.max_transfer_size = 2**23

        # Check that the two devices are not the same
        assert (self.mm2s_device.py_dma_device !=
                self.s2mm_device.py_dma_device)

    def tearDown(self):
        del self.mm2s_device
        del self.s2mm_device

    def run_dma_and_check(self, src_offset, dest_offset, transfer_size,
                          test_data, s2mm_first=True):

        # Create expected result array
        expected_result = test_data

        # Write the test data to MM2S memory
        self.mm2s_device.write_memory_contents(test_data, src_offset)

        # Clear the receiving memory
        self.s2mm_device.write_memory_contents(
            np.zeros((transfer_size,), dtype='uint8'), dest_offset)

        if s2mm_first:
            # Set up an S2MM DMA transfer
            s2mm_do_dma_result = self.s2mm_device.do_dma(
                direction.S2MM, dest_offset, transfer_size)
            # Set up an MM2S DMA transfer
            mm2s_do_dma_result = self.mm2s_device.do_dma(
                direction.MM2S, src_offset, transfer_size)
        else:
            # Set up an MM2S DMA transfer
            mm2s_do_dma_result = self.mm2s_device.do_dma(
                direction.MM2S, src_offset, transfer_size)
            # Set up an S2MM DMA transfer
            s2mm_do_dma_result = self.s2mm_device.do_dma(
                direction.S2MM, dest_offset, transfer_size)

        self.assertEqual(s2mm_do_dma_result, 0)
        self.assertEqual(mm2s_do_dma_result, 0)

        # Wait for mm2s to complete
        mm2s_wait_result = (
            self.mm2s_device.dma_interrupt_wait(direction.MM2S, 1000))
        # Wait for s2mm to complete
        s2mm_wait_result = (
            self.s2mm_device.dma_interrupt_wait(direction.S2MM, 1000))

        self.assertEqual(s2mm_wait_result, transfer_size)
        self.assertEqual(mm2s_wait_result, transfer_size)

        # Read the s2mm memory
        result = self.s2mm_device.read_memory_contents(
            transfer_size, dest_offset)

        # Check the DMA has written back the test data into s2mm memory
        self.assertTrue(np.array_equal(expected_result, result))


    def test_dma_transfer(self):
        '''The system should be able to transfer data from MM2S memory to S2MM
        memory via the PL, such that:

            * 8 bytes <= transfer size < 8 Mbytes
            * Transfer size is a multiple of 8

        The system should be able to transfer data from any source location in
        the MM2S memory to any destination location in the S2MM memory, such
        that:

            * 0 <= source location < MM2S memory size
            * Source location is a multiple of 8
            * 0 <= destination location < S2MM memory size
            * Destination location is a multiple of 8

        The system should be able to do multiple back to back DMA
        transfers.'''

        # This test uses DMA to transfer 8 bytes from the highest available
        # source offset to the highest possble destination offset. Then
        # (self.max_transfer_size - 8) bytes from source offset 0 to
        # destination offset 0. Then eight random amounts of  data from a
        # random source offset in the mm2s memory to the PL and then back into
        # a random destination offset in the s2mm memory.

        sizes = (8, (self.max_transfer_size - 8)) + tuple((np.random.randint(
            8, self.max_transfer_size, 10)//8)*8)

        src_generator = lambda size: (np.random.randint(
            1, (self.mm2s_mem_size - 1 - size))//8)*8
        dest_generator = lambda size: (np.random.randint(
            1, (self.s2mm_mem_size - 1 - size))//8)*8

        srcs = ((((self.mm2s_mem_size - 1 - 8)//8)*8, 0) +
                tuple(src_generator(size) for size in sizes[2:]))
        dests = ((((self.s2mm_mem_size - 1 - 8)//8)*8, 0) +
                tuple(dest_generator(size) for size in sizes[2:]))

        for transfer_size, src_offset, dest_offset in zip(sizes, srcs, dests):
            # Set up test data.
            test_data = np.uint8(np.random.randint(2**8, size=transfer_size))

            # Run the DMA and check the data transferred correctly
            self.run_dma_and_check(src_offset, dest_offset, transfer_size,
                                   test_data)

    def test_cache_coherency(self):
        '''The system should be cache coherent after a transfer.'''

        # This test uses DMA to transfer data from mm2s memory to the PL and
        # then back into s2mm memory. A second DMA transfer is then performed
        # to overwrite the part of the newly transferred data with another
        # section of the original data. When the data is read from destination
        # s2mm memory, it should match the original data with one section
        # modified.
        #
        # Example:
        # ********
        # Original data in mm2s:               |__1__|__2__|__3__|__4__|
        #
        # Data in s2mm after initial transfer: |__1__|__2__|__3__|__4__|
        #
        # Second transfer just reads out section 4 and places it in section 2.
        #
        # Data in s2mm after second transfer:  |__1__|__4__|__3__|__4__|
        #
        # When the userspace application then reads the full data from the
        # s2mm memory, it should read the data correctly. Ie the cache has
        # been updated as part of the transfer.

        for x in range(0, 4):
            # Set up test data
            transfer_size = (
                np.random.randint(8, self.max_transfer_size)//8)*8
            test_data = np.uint8(np.random.randint(2**8, size=transfer_size))

            # Set the source and destination offset
            src_offset = (np.random.randint(
                0, (self.mm2s_mem_size - 1 - transfer_size))//8)*8
            dest_offset = (np.random.randint(
                0, (self.s2mm_mem_size - 1 - transfer_size))//8)*8

            # Define the size of the overwriting data, where it will be taken
            # from in the MM2S memory and where it will go in the S2MM memory
            overwriting_size = (np.random.randint(8, transfer_size)//8)*8
            overwriting_src = (np.random.randint(
                0, (transfer_size - overwriting_size))//8)*8
            overwriting_dest = (np.random.randint(
                0, (transfer_size - overwriting_size))//8)*8
            overwriting_src_offset = overwriting_src + src_offset
            overwriting_dest_offset = overwriting_dest + dest_offset

            # Run the initial DMA and check the data transferred correctly
            self.run_dma_and_check(src_offset, dest_offset, transfer_size,
                                   test_data)

            # Set up an S2MM DMA transfer to copy the last 256 bytes to 256
            # offset
            self.s2mm_device.do_dma(direction.S2MM, overwriting_dest_offset,
                                    overwriting_size)
            # Set up an MM2S DMA transfer for the last 256 bytes
            self.mm2s_device.do_dma(direction.MM2S, overwriting_src_offset,
                                    overwriting_size)

            # Wait for MM2S DMA transfer to complete
            self.mm2s_device.dma_interrupt_wait(direction.MM2S, 1000)
            # Wait for S2MM DMA transfer to complete
            self.s2mm_device.dma_interrupt_wait(direction.S2MM, 1000)

            # Create expected result array
            expected_result = test_data.copy()
            expected_result[
                overwriting_dest:(overwriting_dest +
                                  overwriting_size)] = expected_result[
                                      overwriting_src:(overwriting_src +
                                                       overwriting_size)]

            # Read the s2mm memory
            result = self.s2mm_device.read_memory_contents(
                transfer_size, dest_offset)

            # Check the DMA has written back the test data into s2mm memory
            self.assertTrue(np.array_equal(expected_result, result))

    def test_correct_destination_address(self):
        '''The system should not overwrite any addresses other than those
        specified in the DMA transfer.'''

        # This test uses DMA to transfer data from mm2s memory to the PL and
        # then back into four different s2mm memory locations. Each write
        # should only affect the memory locations specified.

        for x in range(0, 4):
            transfer_size = (
                np.random.randint(8, self.max_transfer_size)//8)*8

            # Set the source offset
            dest_offset = (np.random.randint(
                0, (self.s2mm_mem_size - 1 - transfer_size))//8)*8
            src_offset = (np.random.randint(
                0, (self.mm2s_mem_size - 1 - transfer_size))//8)*8

            # Set up test data.
            test_data = np.uint8(np.random.randint(2**8, size=transfer_size))

            # Create expected result array
            expected_result = np.zeros((self.s2mm_mem_size,), dtype='uint8')
            expected_result[
                (dest_offset):(dest_offset+transfer_size)] = test_data

            # Clear the entire receiving memory
            self.s2mm_device.write_memory_contents(
                np.zeros((self.s2mm_mem_size,), dtype='uint8'), 0)

            # Run the DMA and check the data transferred correctly
            self.run_dma_and_check(src_offset, dest_offset, transfer_size,
                                   test_data)

            # Read the s2mm memory
            result = self.s2mm_device.read_memory_contents(
                self.s2mm_mem_size, 0)

            # Check that the DMA has only written to the section of data
            # specified
            self.assertTrue(np.array_equal(expected_result, result))


    def test_invalid_transfer_size(self):
        '''The system should return INSIZ (Invalid size) if the user
        tries to initiate a DMA transfer with an invalid transfer size as
        specified in :py:func:`test_dma_transfer`.'''

        # This test simulates the situation in which the user tries to
        # initiate a DMA with a size outside of the range 8 bytes -
        # (self.max_transfer_size - 8) bytes and with a transfer size which is
        # not a multiple of 8.

        sizes = (0, 7, (self.max_transfer_size - 7), np.random.randint(0, 8),
                 np.random.randint((self.max_transfer_size - 7), (2**25)),
                 ((np.random.randint(8, self.max_transfer_size)//8)*8 +
                  np.random.randint(1, 8)))

        for transfer_size in sizes:
            # Set the source and destination offset
            src_offset = (np.random.randint(
                0, (self.mm2s_mem_size - 1 - transfer_size))//8)*8

            # Create expected result
            expected_result = errors.INSIZ

            # Set up an MM2S DMA transfer
            result = self.mm2s_device.do_dma(
                direction.MM2S, src_offset, transfer_size)

            # Check the correct error code is returned
            self.assertTrue(result==expected_result)

    def test_invalid_offset(self):
        '''The system should return Inoff (Invalid offset) if the user tries
        to initiate a transfer with an invalid source or destination offset as
        specified in :py:func:`test_dma_transfer`.'''

        # This test simulates the situation in which a user attempts to
        # initiate a DMA transfer with an offset outside the permitted
        # range and with an offset which is not a multiple of 8.

        offset_list = (self.mm2s_mem_size,
                       np.random.randint(self.mm2s_mem_size, (2**28)),
                       ((np.random.randint(8, self.mm2s_mem_size)//8)*8 +
                        np.random.randint(1, 8)))

        for offset in offset_list:
            transfer_size = (
                np.random.randint(8, self.max_transfer_size)//8)*8

            # Create expected result array
            expected_result = errors.INOFF

            # Set up an MM2S DMA transfer
            result = self.mm2s_device.do_dma(
                direction.MM2S, offset, transfer_size)

            # Check the correct error code is returned
            self.assertTrue(result==expected_result)

    def test_invalid_direction(self):
        '''The system should return INDIR (Invalid direction) if the user
        tries to initiate a transfer in a direction which the device is not
        capable.'''

        # This test simulates the situation in which a user attempts to
        # initiate an MM2S transfer in an S2MM only device and then vice versa

        for x in range(0, 2):
            transfer_size = (
                np.random.randint(8, self.max_transfer_size)//8)*8

            offset = (np.random.randint(
                0, (self.mm2s_mem_size - transfer_size))//8)*8

            # Create expected result array
            expected_result = errors.INDIR

            if x == 0:
                # Try to set up an S2MM DMA transfer with an MM2S only device
                result = self.mm2s_device.do_dma(
                    direction.S2MM, offset, transfer_size)
            else:
                # Try to set up an MM2S DMA transfer with an S2MM only device
                result = self.s2mm_device.do_dma(
                    direction.MM2S, offset, transfer_size)

            # Check the correct error code is returned
            self.assertTrue(result==expected_result)

    def test_memory_overrun(self):
        '''The system should return MEMOVER (Memory overrun) if the user tries
        to initiate a DMA transfer with a transfer size and offset which would
        cause the DMA controller to overrun the end of the device memory.'''

        # This test simulates the situation in which the user tries to
        # initiate a DMA with a size and offset which would cause the DMA
        # controller to overrun the end of the device memory.

        for x in range(0, 5):
            transfer_size = (
                np.random.randint(8, self.max_transfer_size)//8)*8

            # Set the source and destination offset
            src_offset = (np.random.randint(
                (self.mm2s_mem_size - transfer_size),
                (self.mm2s_mem_size - 1))//8)*8

            # Create expected result
            expected_result = errors.MEMOVER

            # Set up an MM2S DMA transfer
            result = self.mm2s_device.do_dma(
                direction.MM2S, src_offset, transfer_size)

            # Check the correct error code is returned
            self.assertTrue(result==expected_result)

    def test_dma_order(self):
        '''The system should be able to set up the MM2S operation first and
        then the S2MM operation or vice versa.'''

        # This test sets up the S2MM transfer and then the MM2S transfer for
        # the first transfer. It then performs up a second transfer by setting
        # up the MM2S channel and then the S2MM channel. It then performs 8
        # more transfers by randomly selecting which channel to set up first.

        sizes = tuple((np.random.randint(8, self.max_transfer_size, 10)//8)*8)

        i = 0

        for transfer_size in sizes:
            # Set the source and destination offsets
            src_offset = (np.random.randint(
                0, (self.mm2s_mem_size - 1 - transfer_size))//8)*8
            dest_offset = (np.random.randint(
                0, (self.s2mm_mem_size - 1 - transfer_size))//8)*8

            # Set up test data.
            test_data = np.uint8(np.random.randint(2**8, size=transfer_size))

            if i == 0:
                # Run the DMA and check the data transferred correctly
                self.run_dma_and_check(src_offset, dest_offset, transfer_size,
                                       test_data, True)
            elif i == 1:
                # Run the DMA and check the data transferred correctly
                self.run_dma_and_check(src_offset, dest_offset, transfer_size,
                                       test_data, False)
            else:
                # Run the DMA and check the data transferred correctly
                self.run_dma_and_check(src_offset, dest_offset, transfer_size,
                                       test_data,
                                       bool(np.random.randint(0, 2)))

            i = i + 1

    def test_dma_timeout(self):
        ''' The system should not block indefinitely if the S2MM DMA process
        doesn't return. After a timeout period it should return 0. If the
        system has timed out the system should reset the DMA channels to
        return them to a functioning state.

        The S2MM process should hang if the PS sets up an S2MM DMA transaction
        when there is no data availale to transfer.

        The MM2S direction is not a concern as the DMA will just keep reading
        from memory so should never hang.
        '''

        # This test requests five S2MM transfers of random amounts of data.
        # The DMA controller would wait for ever as this test does not supply
        # any data. However the library should timeout and return a 0.
        # Finally it performs one good transfer to ensure the DMA is still
        # functioning correctly.

        for x in range(0, 6):

            # Set the s2mm size.
            s2mm_size = (
                np.random.randint(8, self.max_transfer_size - 8)//8)*8

            # Set the destination offset
            dest_offset = (np.random.randint(
                0, (self.s2mm_mem_size - 1 - s2mm_size))//8)*8

            # Create expected result
            expected_result = 0

            # Set up an S2MM DMA transfer
            self.s2mm_device.do_dma(direction.S2MM, dest_offset, s2mm_size)

            # Wait for s2mm to timeout
            s2mm_result = (
                self.s2mm_device.dma_interrupt_wait(direction.S2MM, 5000))

            # Check the correct error code is returned
            self.assertTrue(s2mm_result==expected_result)

        # Run a good packet of data through the system to check that the DMA
        # is still functioning correctly. ie that it has been reset
        # successfully
        transfer_size = (
            np.random.randint(8, self.max_transfer_size - 8)//8)*8

        # Set the source and destination offset
        src_offset = (np.random.randint(
            0, (self.mm2s_mem_size - 1 - transfer_size))//8)*8
        dest_offset = (np.random.randint(
            0, (self.s2mm_mem_size - 1 - transfer_size))//8)*8

        # Set up test data.
        test_data = np.uint8(np.random.randint(2**8, size=transfer_size))

        # Run the DMA and check the data transferred correctly
        self.run_dma_and_check(src_offset, dest_offset, transfer_size,
                               test_data)


    def test_dma_mismatched_lengths(self):
        ''' The system should return the number of bytes that was actually
        transferred.

        In the event that a program initiates an S2MM transfer requesting more
        data than is available in the incoming data packet, the system will
        return when the DMA engine has transferred the full incoming data
        packet even though this is less than the requested number of bytes.

        The calling function will be expecting the amount requested so the
        system should return the number of bytes that were actually
        transferred.
        '''

        # This test uses DMA to transfer five random amounts of data from a
        # random source offset in the mm2s memory to the PL. It then requests
        # a larger amount of memory is transferred back into a random
        # destination offset in the s2mm memory. Finally it performs one good
        # read to ensure the DMA is still functioning.

        for x in range(0, 6):

            # Set the mm2s size
            mm2s_size = (
                np.random.randint(8, self.max_transfer_size - 16)//8)*8
            # Set the s2mm size. Make sure the s2mm size is larger than the
            # mm2s. This will cause the DMA engine to hang.
            s2mm_size = (
                np.random.randint(mm2s_size, self.max_transfer_size - 8)//8)*8

            # Set the source and destination offset
            src_offset = (np.random.randint(
                0, (self.mm2s_mem_size - 1 - mm2s_size))//8)*8
            dest_offset = (np.random.randint(
                0, (self.s2mm_mem_size - 1 - s2mm_size))//8)*8

            # Set up test data.
            test_data = np.uint8(np.random.randint(2**8, size=mm2s_size))

            # Write the test data to MM2S memory
            self.mm2s_device.write_memory_contents(test_data, src_offset)

            # Clear the receiving memory
            self.s2mm_device.write_memory_contents(
                np.zeros((s2mm_size,), dtype='uint8'), dest_offset)

            transfer_size_difference = s2mm_size - mm2s_size

            # Create expected results
            expected_return_result = mm2s_size
            expected_mem_read_result = np.append(
                test_data, [0]*transfer_size_difference)

            # Set up an MM2S and S2MM DMA transfer
            self.mm2s_device.do_dma(direction.MM2S, src_offset, mm2s_size)
            self.s2mm_device.do_dma(direction.S2MM, dest_offset, s2mm_size)

            # Wait for mm2s to complete
            mm2s_return_result = (
                self.mm2s_device.dma_interrupt_wait(direction.MM2S, 1000))
            # Wait for s2mm to complete
            s2mm_return_result = (
                self.s2mm_device.dma_interrupt_wait(direction.S2MM, 1000))

            # Check the return value is correct
            self.assertTrue(s2mm_return_result==expected_return_result)

            # Read the s2mm memory
            mem_read_result = self.s2mm_device.read_memory_contents(
                s2mm_size, dest_offset)

            # Check the data written to memory is correct
            self.assertTrue(np.array_equal(
                mem_read_result, expected_mem_read_result))

        # Run a good packet of data through the system to check that the DMA
        # is still functioning correctly. ie that it has been reset
        # successfully

        transfer_size = (
            np.random.randint(8, self.max_transfer_size - 8)//8)*8

        # Set the source and destination offset
        src_offset = (np.random.randint(
            0, (self.mm2s_mem_size - 1 - transfer_size))//8)*8
        dest_offset = (np.random.randint(
            0, (self.s2mm_mem_size - 1 - transfer_size))//8)*8

        # Set up test data.
        test_data = np.uint8(np.random.randint(2**8, size=transfer_size))

        # Run the DMA and check the data transferred correctly
        self.run_dma_and_check(src_offset, dest_offset, transfer_size,
                               test_data)
