from libc.stdint cimport uintptr_t, uint32_t, int32_t
from libcpp cimport bool
from posix.unistd cimport readlink

cdef extern from "axi_dma.h":
    ctypedef struct dma_device_t:
        pass
    ctypedef enum dma_direction:
        MM2S_TRANSFER = 0
        S2MM_TRANSFER = 1
    ctypedef enum:
        GANNET_E_INSIZ = -1
        GANNET_E_INOFF = -2
        GANNET_E_INDIR = -3
        GANNET_E_MEMOVER = -4
        GANNET_E_DMAINT = -5
        GANNET_E_SLAVE = -6
        GANNET_E_DECDE = -7
    dma_device_t* gannet_device_init(const char *device_path)
    int gannet_device_cleanup(dma_device_t *dma_device)
    int gannet_do_dma(dma_device_t *dma_device,
                      dma_direction dma_transfer_direction,
                      size_t offset,
                      uint32_t nbytes)
    int gannet_dma_interrupt_wait(dma_device_t *dma_device,
                                  dma_direction dma_transfer_direction,
                                  int32_t timeout_msec)
    void* gannet_get_memory(dma_device_t *dma_device)
    int  gannet_dma_reset(dma_device_t *dma_device,
                          dma_direction dma_transfer_direction)
