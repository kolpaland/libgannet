from libc.stdint cimport uint8_t
from libc.stdint cimport uintptr_t
from libc.string cimport memcpy
from libc.stdio cimport printf
import numpy as np
cimport numpy as np

class errors(object):
    INSIZ = GANNET_E_INSIZ
    INOFF = GANNET_E_INOFF
    INDIR = GANNET_E_INDIR
    MEMOVER = GANNET_E_MEMOVER
    DMAINT = GANNET_E_DMAINT
    SLAVE = GANNET_E_SLAVE
    DECDE = GANNET_E_DECDE

class direction(object):
    MM2S = MM2S_TRANSFER
    S2MM = S2MM_TRANSFER

cdef class DMA:

    cdef dma_device_t *dma_device
    cdef public object py_dma_device

    def __cinit__(self, device_path):
        self.dma_device = gannet_device_init(device_path)
        if self.dma_device == NULL:
            raise RuntimeError('DMA Device Init Failed')
        self.py_dma_device = <uintptr_t>self.dma_device

    def __init__(self, device_path):
        pass

    def __dealloc__(self):
        cleanup_result = gannet_device_cleanup(self.dma_device)
        if cleanup_result != 0:
            raise RuntimeError('DMA Device Cleanup Failed')

    cpdef do_dma(self, dma_transfer_direction, offset, nbytes):
        return gannet_do_dma(self.dma_device, dma_transfer_direction, offset,
                             nbytes)

    cpdef dma_interrupt_wait(self, dma_transfer_direction, timeout_msec):
        return gannet_dma_interrupt_wait(self.dma_device,
                                         dma_transfer_direction,
                                         timeout_msec)

    cpdef dma_reset(self, dma_transfer_direction):
        return gannet_dma_reset(self.dma_device,
                                dma_transfer_direction)

    def read_memory_contents(self, size, int offset):

        read_buffer = np.empty((size,), dtype='uint8')
        cdef uint8_t *mem = <uint8_t *>gannet_get_memory(self.dma_device)

        cdef uint8_t *buffer_pointer = <uint8_t*>np.PyArray_DATA(read_buffer)

        memcpy(buffer_pointer, (mem + offset), size)

        return read_buffer

    def write_memory_contents(self, write_buffer, int offset):
        cdef uint8_t *mem = <uint8_t *>gannet_get_memory(self.dma_device)

        cdef uint8_t *buffer_pointer = <uint8_t*>np.PyArray_DATA(write_buffer)
        cdef int size = len(write_buffer.data)

        memcpy((mem + offset), buffer_pointer, size)

        return 0

def multiple_dma_transfers(device_path_mm2s, device_path_s2mm, offset, nbytes,
                           int repeats=1):

    mm2s_transfer_obj = DMA(device_path_mm2s)
    s2mm_transfer_obj = DMA(device_path_s2mm)

    def repeated_function_closure():

        cdef int i

        for i in range(0, repeats):
            mm2s_transfer_obj.do_dma(0, offset, nbytes)
            s2mm_transfer_obj.do_dma(1, offset, nbytes)
            mm2s_transfer_obj.dma_interrupt_wait(0, 1000)
            s2mm_transfer_obj.dma_interrupt_wait(1, 1000)

    return repeated_function_closure

