#set_property PACKAGE_PIN V14 [get_ports mm2s_int_led]
#set_property IOSTANDARD LVCMOS18 [get_ports mm2s_int_led]
#set_property PACKAGE_PIN T22 [get_ports s2mm_int_led]
#set_property IOSTANDARD LVCMOS18 [get_ports s2mm_int_led]

#set_property PACKAGE_PIN T22 [get_ports fifo_overflow_led]
#set_property IOSTANDARD LVCMOS18 [get_ports fifo_overflow_led]
