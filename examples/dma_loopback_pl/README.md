This README file contains information on the contents of libgannet.

Please see the corresponding sections below for details.


Patches
=======

Please submit any patches against libgannet to the maintainer:

Maintainer: Toby Gomersall <toby.gomersall@smartacoustics.co.uk>


Table of Contents
=================

1. Testing
2. Building
3. Programming 


Testing
=======

Testing should be conducted using libgannet (git@gitlab.com:toby.gomersall/libgannet.git)


Building
========

To load the project, open vivado and enter the following command in to the tcl console:

`source <path to directory>/acp_dma_pl_build.tcl`

Then generate bitstream. This will produce and output .bin file which can be used to program the device.

Programming
===========

This development has proceeded using linux (yocto) running on a Zynq. Copy the .bin file onto the device. Then run the following command to program the PL:

`cat acp_dma_pl_wrapper.bin > /dev/xdevcfg`

