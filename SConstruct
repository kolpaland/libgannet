env = Environment(
    CC='gcc',
    CCFLAGS=['-std=c11', '-Wall', '-Wno-unused-function'])

variants = {'release': {'CCFLAGS': ['-Werror']}}

import os

for variant in variants:
    build_env = env.Clone()
    build_env.AppendUnique(**variants[variant])
    SConscript(os.path.join('src', 'SConscript'), exports=['build_env'],
              variant_dir=variant)

# Build the test wrappers
test_variant = 'release'
test_build_env = env.Clone()
test_build_env.AppendUnique(
    LIBPATH='#/'+test_variant, LIBS='axi_dma',
    RPATH=os.path.join(Dir('#').abspath, test_variant), 
    CPPPATH='#/'+test_variant)

SConscript('test/SConscript', exports=['test_build_env'])

# Valgrind
valgrind_envs = {}
for variant in variants:
    valgrind_env = env.Clone()
    valgrind_env.AppendUnique(
        LIBPATH='#/'+variant, LIBS='axi_dma',
        RPATH=os.path.join(Dir('#').abspath, variant), 
        CPPPATH='#/'+variant)

    valgrind_envs[variant] = valgrind_env

SConscript('valgrind/SConscript', exports=['valgrind_envs'])
