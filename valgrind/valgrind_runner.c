#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include "axi_dma.h"

int main() {
 
    dma_device_t *dma_device = gannet_device_init("/dev/axi_dma_mm2s");

    uint8_t* mem = gannet_get_memory(dma_device);

    printf("%u\n", (uintptr_t)mem);

    for(int i = 0; i<(1<<25); ++i){
        mem[i] = i % (sizeof(*mem) * 8);
    }

    gannet_device_cleanup(dma_device);

}

